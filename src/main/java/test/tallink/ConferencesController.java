package test.tallink;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/conferences")
public class ConferencesController {

	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage() {
		return "ConferencesManagement";
	}

}
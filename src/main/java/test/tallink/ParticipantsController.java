package test.tallink;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/participants/{confId}")
public class ParticipantsController {

	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage(ModelMap map, @PathVariable("confId") String confId) {
		map.addAttribute("confId", confId);
		return "ParticipantsManagement";
	}

}
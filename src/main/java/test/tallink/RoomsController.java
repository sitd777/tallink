package test.tallink;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/rooms")
public class RoomsController {

	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage() {
		return "RoomsManagement";
	}

}
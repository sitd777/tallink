package test.tallink.db;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "Conferences")
public class Conference extends BaseEntity {

	private String roomId;

	private String name;

	private Date date;

	private int seats = 0;

	private int used = 0;

	public Conference() {
	}

	public Conference(String roomId, String name, String date, int seats, int used) {
		super();
		this.setRoomId(roomId);
		this.setName(name);
		this.setDate(date);
		this.setSeats(seats);
		this.setUsed(used);
	}

	public Conference(String roomId, String name, String date, int seats, int used, String ID) {
		super();
		this.setRoomId(roomId);
		this.setName(name);
		this.setDate(date);
		this.setSeats(seats);
		this.setUsed(used);
		this.setId(ID);
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		return format.format(date);
	}

	public void setDate(String date) {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		try {
			this.date = format.parse(date);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getUsed() {
		return used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	@Override
	public String toString() {
		return "Conference [id=" + getId() + ", roomId=" + roomId + ", name=" + name + ", date=" + date + ", seats="
				+ seats + ", used=" + used + "]";
	}
}

package test.tallink.db;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConferenceRepository extends MongoRepository<Conference, String> {

	Conference findById(String id);

}

package test.tallink.db;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "Participants")
public class Participant extends BaseEntity {

	private String conferenceId;

	private String fullname;

	private Date birthdate;

	public Participant() {
	}

	public Participant(String conferenceId, String fullname, String birthdate) {
		super();
		this.setConferenceId(conferenceId);
		this.setFullname(fullname);
		this.setBirthdate(birthdate);
	}

	public Participant(String conferenceId, String fullname, String birthdate, String ID) {
		super();
		this.setConferenceId(conferenceId);
		this.setFullname(fullname);
		this.setBirthdate(birthdate);
		this.setId(ID);
	}

	public String getConferenceId() {
		return conferenceId;
	}

	public void setConferenceId(String conferenceId) {
		this.conferenceId = conferenceId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getBirthdate() {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		return format.format(birthdate);
	}

	public void setBirthdate(String birthdate) {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		try {
			this.birthdate = format.parse(birthdate);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public String toString() {
		return "Participant [id=" + getId() + ", conferenceId=" + conferenceId + ", fullname=" + fullname
				+ ", birthdate=" + birthdate + "]";
	}
}

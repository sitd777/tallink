package test.tallink.db;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ParticipantRepository extends MongoRepository<Participant, String> {

	Participant findById(String id);

	List<Participant> findByConferenceId(String confId);

}

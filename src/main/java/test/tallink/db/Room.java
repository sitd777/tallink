package test.tallink.db;

import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "Rooms")
public class Room extends BaseEntity {

	// @Indexed(unique = true)
	// private String roomId;

	private String name;

	private String location;

	private int seats;

	public Room() {
	}

	public Room(String name, String location, int seats) {
		super();
		this.setName(name);
		this.setLocation(location);
		this.setSeats(seats);
	}

	public Room(String name, String location, int seats, String ID) {
		super();
		this.setName(name);
		this.setLocation(location);
		this.setSeats(seats);
		this.setId(ID);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	@Override
	public String toString() {
		// return "Room [roomId=" + roomId + ", name=" + name + ", location=" +
		// location + ", seats=" + seats + "]";
		return "Room [id=" + getId() + ", name=" + name + ", location=" + location + ", seats=" + seats + "]";
	}

}

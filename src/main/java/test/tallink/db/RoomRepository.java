package test.tallink.db;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoomRepository extends MongoRepository<Room, String> {

	// Room findByRoomId(String roomId);
	Room findById(String id);

}

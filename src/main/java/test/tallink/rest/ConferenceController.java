package test.tallink.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.tallink.db.Conference;
import test.tallink.db.ConferenceRepository;
import test.tallink.db.Room;
import test.tallink.db.RoomRepository;

@RestController
public class ConferenceController {

	private RoomRepository roomRepo;

	private ConferenceRepository confRepo;

	@Autowired
	public ConferenceController(ConferenceRepository confRepo, RoomRepository roomRepo) {
		super();
		this.setConferenceRepo(confRepo);
		this.setRoomRepo(roomRepo);
	}

	@PostConstruct
	public void initData() {
		confRepo.deleteAll();
		confRepo.save(new Conference("1", "Java Conference", "21.01.2017", 100, 3, "1"));
		confRepo.save(new Conference("2", "Spring Conference", "05.03.2017", 2, 1, "2"));
	}

	@RequestMapping(value = "/conference", method = RequestMethod.GET)
	public List<Conference> getAllConferences() {
		return confRepo.findAll();
	}

	@RequestMapping(value = "/conference/{confId}", method = RequestMethod.GET)
	public Conference getConference(@PathVariable("confId") String confId) {
		return confRepo.findById(confId);
	}

	@RequestMapping(value = "/conference", method = RequestMethod.POST)
	public Conference createConference(@RequestBody Conference conf) {
		String roomId = conf.getRoomId();
		Room room = this.roomRepo.findById(roomId);
		if (room != null) {
			conf.setSeats(room.getSeats());
		}
		confRepo.save(conf);
		return conf;
	}

	@RequestMapping(value = "/conference/{confId}", method = RequestMethod.PUT)
	public Conference updateConference(@PathVariable("confId") String confId, @RequestBody Conference conf) {
		Conference confDb = confRepo.findById(confId);
		// confDb.setRoomId(conf.getRoomId());
		confDb.setName(conf.getName());
		confDb.setDate(conf.getDate());
		confRepo.save(confDb);
		return confDb;
	}

	@RequestMapping(value = "/conference/{confId}", method = RequestMethod.DELETE)
	public Conference deleteConference(@PathVariable("confId") String confId) {
		Conference conf = confRepo.findById(confId);
		confRepo.delete(conf);
		return conf;
	}

	public ConferenceRepository getConferenceRepo() {
		return confRepo;
	}

	public void setConferenceRepo(ConferenceRepository confRepo) {
		this.confRepo = confRepo;
	}

	public RoomRepository getRoomRepo() {
		return roomRepo;
	}

	public void setRoomRepo(RoomRepository roomRepo) {
		this.roomRepo = roomRepo;
	}
}

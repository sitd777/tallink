package test.tallink.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.tallink.db.Conference;
import test.tallink.db.ConferenceRepository;
import test.tallink.db.Participant;
import test.tallink.db.ParticipantRepository;

@RestController
public class ParticipantController {

	private ConferenceRepository confRepo;

	private ParticipantRepository partRepo;

	@Autowired
	public ParticipantController(ParticipantRepository partRepo, ConferenceRepository confRepo) {
		super();
		this.setParticipantRepo(partRepo);
		this.setConferenceRepo(confRepo);
	}

	@PostConstruct
	public void initData() {
		partRepo.deleteAll();
		partRepo.save(new Participant("1", "Maksim Rybchenko", "29.07.1981", "1"));
		partRepo.save(new Participant("1", "Igor Petrov", "01.08.1977", "2"));
		partRepo.save(new Participant("1", "Mark Uus", "05.08.1975", "3"));
		partRepo.save(new Participant("2", "Ivar Kask", "10.01.1982", "4"));
	}

	@RequestMapping(value = "/participant/byconf/{confId}", method = RequestMethod.GET)
	public List<Participant> getConfParticipants(@PathVariable("confId") String confId) {
		return partRepo.findByConferenceId(confId);
	}

	@RequestMapping(value = "/participant", method = RequestMethod.GET)
	public List<Participant> getAllParticipants() {
		return partRepo.findAll();
	}

	@RequestMapping(value = "/participant/{partId}", method = RequestMethod.GET)
	public Participant getParticipant(@PathVariable("partId") String partId) {
		return partRepo.findById(partId);
	}

	@RequestMapping(value = "/participant", method = RequestMethod.POST)
	public Participant createParticipant(@RequestBody Participant part) {
		String confId = part.getConferenceId();
		Conference conf = this.confRepo.findById(confId);
		if (conf == null || conf.getSeats() <= conf.getUsed())
			return null;
		partRepo.save(part);
		this.recalcParticipants(confId);
		return part;
	}

	@RequestMapping(value = "/participant/{partId}", method = RequestMethod.PUT)
	public Participant updateParticipant(@PathVariable("partId") String partId, @RequestBody Participant part) {
		Participant partDb = partRepo.findById(partId);
		// partDb.setparterenceId(part.getparterenceId());
		partDb.setFullname(part.getFullname());
		partDb.setBirthdate(part.getBirthdate());
		partRepo.save(partDb);
		return partDb;
	}

	@RequestMapping(value = "/participant/{partId}", method = RequestMethod.DELETE)
	public Participant deleteParticipant(@PathVariable("partId") String partId) {
		Participant part = partRepo.findById(partId);
		partRepo.delete(part);
		this.recalcParticipants(part.getConferenceId());
		return part;
	}

	public ParticipantRepository getParticipantRepo() {
		return partRepo;
	}

	public void setParticipantRepo(ParticipantRepository partRepo) {
		this.partRepo = partRepo;
	}

	public ConferenceRepository getConferenceRepo() {
		return confRepo;
	}

	public void setConferenceRepo(ConferenceRepository confRepo) {
		this.confRepo = confRepo;
	}

	public void recalcParticipants(String confId) {
		Conference conf = this.confRepo.findById(confId);
		if (conf != null) {
			List<Participant> parts = this.partRepo.findByConferenceId(confId);
			conf.setUsed(parts.size());
			this.confRepo.save(conf);
		}
	}
}

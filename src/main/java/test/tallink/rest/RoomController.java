package test.tallink.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.tallink.db.Room;
import test.tallink.db.RoomRepository;

@RestController
public class RoomController {

	private RoomRepository roomRepo;

	@Autowired
	public RoomController(RoomRepository roomRepo) {
		super();
		this.setRoomRepo(roomRepo);
	}

	@PostConstruct
	public void initData() {
		roomRepo.deleteAll();
		roomRepo.save(new Room("Conference room #1", "M/S Baltic Queen", 100, "1"));
		roomRepo.save(new Room("Conference room #2", "M/S Baltic Queen", 2, "2"));
	}

	@RequestMapping(value = "/room", method = RequestMethod.GET)
	public List<Room> getAllRooms() {
		return roomRepo.findAll();
	}

	@RequestMapping(value = "/room/{roomId}", method = RequestMethod.GET)
	public Room getRoom(@PathVariable("roomId") String roomId) {
		return roomRepo.findById(roomId);
	}

	@RequestMapping(value = "/room", method = RequestMethod.POST)
	public Room createRoom(@RequestBody Room room) {
		roomRepo.save(room);
		return room;
	}

	@RequestMapping(value = "/room/{roomId}", method = RequestMethod.PUT)
	public Room updateRoom(@PathVariable("roomId") String roomId, @RequestBody Room room) {
		Room rooms = roomRepo.findById(roomId);
		rooms.setName(room.getName());
		rooms.setLocation(room.getLocation());
		rooms.setSeats(room.getSeats());
		roomRepo.save(rooms);
		return rooms;
	}

	@RequestMapping(value = "/room/{roomId}", method = RequestMethod.DELETE)
	public Room deleteRoom(@PathVariable("roomId") String roomId) {
		Room room = roomRepo.findById(roomId);
		roomRepo.delete(room);
		return room;
	}

	public RoomRepository getRoomRepo() {
		return roomRepo;
	}

	public void setRoomRepo(RoomRepository roomRepo) {
		this.roomRepo = roomRepo;
	}
}

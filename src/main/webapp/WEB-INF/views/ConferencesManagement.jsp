<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Tallink test: Conferences</title>  
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"></link> -->
     <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/css/jquery.ui.all.css"></link> -->
     <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"></link>
     <!-- <link href="<c:url value='/static/css/jquery.ui.all.min.css' />" rel="stylesheet"></link> -->
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="ConfController as ctrl">
      
		<ul class="nav nav-pills">
		 	<li class="active"><a href="/conferences" class="btn btn-default">Conference list</a></li>
		  	<li><a href="/rooms" class="btn btn-default">Room list</a></li>
		</ul>
      
          <div class="panel panel-default margintop15">
              <div class="panel-heading"><span class="lead">Add/Edit Form</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.conf.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.conf.name" name="name" class="form-control input-sm" placeholder="Enter conference name" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.name.$error.required">This is a required field</span>
                                      <span ng-show="myForm.name.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.name.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Date</label>
                              <div class="col-md-7">
  						          <input type="text" ng-model="ctrl.conf.date" name="date" class="form-control input-sm" placeholder="Enter conference date (dd.mm.yyyy)" bs-datepicker data-date-type="string" data-date-format="dd.MM.yyyy" data-autoclose="1" required/>
                              
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.date.$error.required">This is a required field</span>
                                      <span ng-show="myForm.date.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Room</label>
                              <div class="col-md-7">
                                  <select name="roomId" class="form-control input-sm" ng-model="ctrl.conf.roomId" ng-disabled="ctrl.conf.id != null" ng-readonly="ctrl.conf.id != null" required>
                                  	  <option ng-repeat="r in ctrl.rooms" ng-selected="r.id == ctrl.conf.roomId" value="{{r.id}}">{{ r.name }} / {{ r.location }} / {{ r.seats }}</option>
                                  </select>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.roomId.$error.required">This is a required field</span>
                                      <span ng-show="myForm.roomId.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.conf.id? 'Add' : 'Update'}}" class="btn btn-primary btn-sm btn-submit" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Conferences </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Date</th>
                              <th>Room</th>
                              <th>Seats left</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.confs">
                              <td><span ng-bind="u.name"></span></td>
                              <td><span ng-bind="u.date"></span></td>
                              <td><span>{{ ctrl.getRoomName(u.roomId) }}</span></td>
                              <td><span>{{ u.seats - u.used }} / {{ u.seats }}</span></td>
                              <td><nobr>
                              <a href="/participants/{{u.id}}" class="btn btn-primary">Participants</a>
                              <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success">Edit</button>
                              <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger">Remove</button>
                              </nobr></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script> -->
      <script src="<c:url value='/static/js/angular.min.js' />"></script>
      <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.min.js"></script> -->
	  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.tpl.min.js"></script> -->
      <script src="<c:url value='/static/js/angular-strap.min.js' />"></script>  
      <script src="<c:url value='/static/js/angular-strap.tpl.min.js' />"></script>
      
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script src="<c:url value='/static/js/service/conf_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/conf_controller.js' />"></script>
  </body>
</html>
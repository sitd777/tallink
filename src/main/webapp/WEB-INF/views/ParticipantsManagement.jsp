<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Tallink test: Participants</title>  
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"></link> -->
     <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/css/jquery.ui.all.css"></link> -->
     <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"></link>
     <!-- <link href="<c:url value='/static/css/jquery.ui.all.min.css' />" rel="stylesheet"></link> -->
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="PartController as ctrl">
      
		<ul class="nav nav-pills">
		 	<li><a href="/conferences" class="btn btn-default">Conference list</a></li>
		  	<li><a href="/rooms" class="btn btn-default">Room list</a></li>
		</ul>
      
          <div class="panel panel-default margintop15">
              <div class="panel-heading"><span class="lead">Conference</span></div>
              <div class="tablecontainer margintop15">
              	  <div class="row">
                  	  <div class="form-group col-md-6">
                          <label class="col-md-4 control-lable">Conference name</label>
                          <div class="col-md-8">{{ ctrl.conf.name }}</div>
                      </div>
                      <div class="form-group col-md-6">
                          <label class="col-md-4 control-lable">Conference date</label>
                          <div class="col-md-8">{{ ctrl.conf.date }}</div>
                      </div>
                  </div>
              	  <div class="row">
                  	  <div class="form-group col-md-6">
                          <label class="col-md-4 control-lable">Seats total</label>
                          <div class="col-md-8">{{ ctrl.conf.seats }}</div>
                      </div>
                      <div class="form-group col-md-6">
                          <label class="col-md-4 control-lable">Seats left</label>
                          <div class="col-md-8">
                          		<div class="seats-left">{{ ctrl.conf.seats - ctrl.conf.used }}</div>
                          		<div class="has-error" ng-show="ctrl.createError"><span>Maximum number of seats is used!</span></div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      
          <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Add/Edit Form</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.part.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Paticipant Fullname</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.part.fullname" name="fullname" class="form-control input-sm" placeholder="Enter participant name" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.fullname.$error.required">This is a required field</span>
                                      <span ng-show="myForm.fullname.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.fullname.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Birth Date</label>
                              <div class="col-md-7">
  						          <input type="text" ng-model="ctrl.part.birthdate" name="birthdate" class="form-control input-sm" placeholder="Enter participant birthdate (dd.mm.yyyy)" bs-datepicker data-date-type="string" data-date-format="dd.MM.yyyy" data-autoclose="1" required/>
                              
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.birthdate.$error.required">This is a required field</span>
                                      <span ng-show="myForm.birthdate.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.part.id? 'Add' : 'Update'}}" class="btn btn-primary btn-sm btn-submit" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Participants </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Full Name</th>
                              <th>Birth Date</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.parts">
                              <td><span ng-bind="u.fullname"></span></td>
                              <td><span ng-bind="u.birthdate"></span></td>
                              <td><nobr>
                              <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success">Edit</button>
                              <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger">Remove</button>
                              </nobr></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script> -->
      <script src="<c:url value='/static/js/angular.min.js' />"></script>
      <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.min.js"></script> -->
	  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.tpl.min.js"></script> -->
      <script src="<c:url value='/static/js/angular-strap.min.js' />"></script>  
      <script src="<c:url value='/static/js/angular-strap.tpl.min.js' />"></script>
      
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script>let CONFID = '${confId}';</script>
      <script src="<c:url value='/static/js/service/part_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/part_controller.js' />"></script>
  </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Tallink test: Rooms</title>  
     <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"></link>
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="RoomController as ctrl">
      
		<ul class="nav nav-pills">
		 	<li><a href="/conferences" class="btn btn-default">Conference list</a></li>
		  	<li class="active"><a href="/rooms" class="btn btn-default">Room list</a></li>
		</ul>
      	  
          <div class="panel panel-default margintop15">
              <div class="panel-heading"><span class="lead">Add/Edit Form</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.room.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.room.name" name="name" class="form-control input-sm" placeholder="Enter room name" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.name.$error.required">This is a required field</span>
                                      <span ng-show="myForm.name.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.name.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                        
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Location</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.room.location" name="location" class="form-control input-sm" placeholder="Enter room location" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.location.$error.required">This is a required field</span>
                                      <span ng-show="myForm.location.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.location.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Seats</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.room.seats" name="seats" class="form-control input-sm" placeholder="Enter number of seats in room" required/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.seats.$error.required">This is a required field</span>
                                      <span ng-show="myForm.seats.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.room.id? 'Add' : 'Update'}}" class="btn btn-primary btn-sm btn-submit" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Rooms </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Location</th>
                              <th>Seats</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.rooms">
                              <td><span ng-bind="u.name"></span></td>
                              <td><span ng-bind="u.location"></span></td>
                              <td><span ng-bind="u.seats"></span></td>
                              <td>
                              <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>  
                              <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script> -->
      <script src="<c:url value='/static/js/angular.min.js' />"></script>
      <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.min.js"></script> -->
	  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.3.8/angular-strap.tpl.min.js"></script> -->
      <script src="<c:url value='/static/js/angular-strap.min.js' />"></script>  
      <script src="<c:url value='/static/js/angular-strap.tpl.min.js' />"></script>
            
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script src="<c:url value='/static/js/service/room_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/room_controller.js' />"></script>
  </body>
</html>
class ConfController {
    constructor($scope, ConfService) {
    	this._$scope = $scope;
    	this._ConfService = ConfService;
    	
        this.conf = {id:null, name:'', date:'', roomId:0};
        this.confs = [];
        this.rooms = [];
        
        this.fetchAllRooms();
        this.fetchAllConfs();
    }
    
    getRoomName(roomId) {
    	for(let room of this.rooms) {
    		if(room.id != roomId) continue;
    		return room.name;
    	}
    	return '-';
    }
    
    fetchAllRooms() {
    	let self = this;
    	this._ConfService.fetchAllRooms()
            .then(
	            function(d) {
	                self.rooms = d;
	            },
	            function(errResponse){
	                console.error('Error while fetching Rooms');
	            }
	        );
    }

    fetchAllConfs() {
    	let self = this;
    	this._ConfService.fetchAllConfs()
            .then(
	            function(d) {
	                self.confs = d;
	            },
	            function(errResponse){
	                console.error('Error while fetching Conferences');
	            }
	        );
    }

    createConf(conf) {
    	let self = this;
    	this._ConfService.createConf(conf)
            .then(
	            function() {
	            	self.fetchAllConfs();
	            },
	            function(errResponse){
	                console.error('Error while creating Conference');
	            }
	        );
    }

    updateConf(conf, id) {
    	let self = this;
    	this._ConfService.updateConf(conf, id)
            .then(
	            function() {
	            	self.fetchAllConfs();
	            },
	            function(errResponse){
	                console.error('Error while updating Conference');
	            }
	        );
    }

    deleteConf(id) {
    	let self = this;
    	this._ConfService.deleteConf(id)
            .then(
	            function() {
	            	self.fetchAllConfs();
	            },
	            function(errResponse){
	                console.error('Error while deleting Conference');
	            }
	        );
    }

    submit() {
        if(this.conf.id===null){
            console.log('Saving New Conference', this.conf);
            this.createConf(this.conf);
        }else{
        	this.updateConf(this.conf, this.conf.id);
            console.log('Conference updated with id ', this.conf.id);
        }
        this.reset();
    }

    edit(id) {
        console.log('id to be edited', id);
        for(var i = 0; i < this.confs.length; i++){
            if(this.confs[i].id === id) {
            	this.conf = angular.copy(this.confs[i]);
                break;
            }
        }
    }

    remove(id) {
        console.log('id to be deleted', id);
        if(this.conf.id === id) {//clean form if the user to be deleted is shown there.
        	this.reset();
        }
        this.deleteConf(id);
    }


    reset() {
    	this.conf = {id:null, name:'', date:'', roomId:0};
        this._$scope.myForm.$setPristine(); //reset Form
    }
}

angular.module('myApp').controller('ConfController', ['$scope', 'ConfService', ($scope, ConfService) => new ConfController($scope, ConfService)]);


class PartController {
    constructor($scope, PartService) {
    	this._$scope = $scope;
    	this._PartService = PartService;
    	
        this.part = {id:null, fullname:'', birthdate:'', conferenceId:0};
        this.parts = [];
        this.conf = null;
        
        this.createError = false;

        this.fetchAllParts();
    }

    fetchAllParts() {
    	let self = this;
    	this._PartService.fetchAllParts()
            .then(
	            function(d) {
	                self.parts = d;
	            },
	            function(errResponse){
	                console.error('Error while fetching Participants');
	            }
	        );
    	this._PartService.getConf()
	        .then(
	            function(d) {
	                self.conf = d;
	            },
	            function(errResponse){
	                console.error('Error while fetching Conference');
	            }
	        );
    }

    createPart(part) {
    	let self = this;
    	this._PartService.createPart(part)
            .then(
	            function(res) {
	            	self.createError = !res;
	            	self.fetchAllParts();
	            },
	            function(errResponse){
	                console.error('Error while creating Participant');
	            }
	        );
    }

    updatePart(part, id) {
    	let self = this;
    	this._PartService.updatePart(part, id)
            .then(
	            function() {
	            	self.createError = false;
	            	self.fetchAllParts();
	            },
	            function(errResponse){
	                console.error('Error while updating Participant');
	            }
	        );
    }

    deletePart(id) {
    	let self = this;
    	this._PartService.deletePart(id)
            .then(
	            function() {
	            	self.createError = false;
	            	self.fetchAllParts();
	            },
	            function(errResponse){
	                console.error('Error while deleting Participant');
	            }
	        );
    }

    submit() {
        if(this.part.id===null){
            console.log('Saving New Participant', this.part);
            this.createPart(this.part);
        }else{
        	this.updatePart(this.part, this.part.id);
            console.log('Participant updated with id ', this.part.id);
        }
        this.reset();
    }

    edit(id) {
        console.log('id to be edited', id);
        for(var i = 0; i < this.parts.length; i++){
            if(this.parts[i].id === id) {
            	this.part = angular.copy(this.parts[i]);
                break;
            }
        }
    }

    remove(id) {
        console.log('id to be deleted', id);
        if(this.part.id === id) {//clean form if the user to be deleted is shown there.
        	this.reset();
        }
        this.deletePart(id);
    }


    reset() {
    	this.part = {id:null, fullname:'', birthdate:'', conferenceId:0};
        this._$scope.myForm.$setPristine(); //reset Form
    }
}

angular.module('myApp').controller('PartController', ['$scope', 'PartService', ($scope, PartService) => new PartController($scope, PartService)]);


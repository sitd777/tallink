class RoomController {
    constructor($scope, RoomService) {
    	this._$scope = $scope;
    	this._RoomService = RoomService;
    	
        this.room = {id:null, name:'', location:'', seats:0};
        this.rooms = [];
        this.fetchAllRooms();
    }

    fetchAllRooms() {
    	let self = this;
    	this._RoomService.fetchAllRooms()
            .then(
	            function(d) {
	                self.rooms = d;
	            },
	            function(errResponse){
	                console.error('Error while fetching Rooms');
	            }
	        );
    }

    createRoom(room) {
    	let self = this;
    	this._RoomService.createRoom(room)
            .then(
	            function() {
	            	self.fetchAllRooms();
	            },
	            function(errResponse){
	                console.error('Error while creating Room');
	            }
	        );
    }

    updateRoom(room, id) {
    	let self = this;
    	this._RoomService.updateRoom(room, id)
            .then(
	            function() {
	            	self.fetchAllRooms();
	            },
	            function(errResponse){
	                console.error('Error while updating Room');
	            }
	        );
    }

    deleteRoom(id) {
    	let self = this;
    	this._RoomService.deleteRoom(id)
            .then(
	            function() {
	            	self.fetchAllRooms();
	            },
	            function(errResponse){
	                console.error('Error while deleting Room');
	            }
	        );
    }

    submit() {
        if(this.room.id===null){
            console.log('Saving New Room', this.room);
            this.createRoom(this.room);
        }else{
        	this.updateRoom(this.room, this.room.id);
            console.log('Room updated with id ', this.room.id);
        }
        this.reset();
    }

    edit(id) {
        console.log('id to be edited', id);
        for(var i = 0; i < this.rooms.length; i++){
            if(this.rooms[i].id === id) {
            	this.room = angular.copy(this.rooms[i]);
                break;
            }
        }
    }

    remove(id) {
        console.log('id to be deleted', id);
        if(this.room.id === id) {//clean form if the user to be deleted is shown there.
        	this.reset();
        }
        this.deleteRoom(id);
    }


    reset() {
    	this.room = {id:null, name:'', location:'', seats:0};
        this._$scope.myForm.$setPristine(); //reset Form
    }
}

angular.module('myApp').controller('RoomController', ['$scope', 'RoomService', ($scope, RoomService) => new RoomController($scope, RoomService)]);


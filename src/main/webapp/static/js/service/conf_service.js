class ConfService {
	
    constructor($http, $q) {
    	this.REST_SERVICE_URI = 'http://localhost:8080/conference/';
    	this.REST_SERVICE_URI_ROOM = 'http://localhost:8080/room/';
    	this._$q = $q;
    	this._$http = $http;
    }
    
    fetchAllRooms() {
        let deferred = this._$q.defer();
        this._$http.get(this.REST_SERVICE_URI_ROOM)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Rooms');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    fetchAllConfs() {
        let deferred = this._$q.defer();
        this._$http.get(this.REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Conferences');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    createConf(conf) {
        let deferred = this._$q.defer();
        this._$http.post(this.REST_SERVICE_URI, conf)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Conference');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }


    updateConf(conf, id) {
        let deferred = this._$q.defer();
        this._$http.put(this.REST_SERVICE_URI + id, conf)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating Conference');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    deleteConf(id) {
        let deferred = this._$q.defer();
        this._$http.delete(this.REST_SERVICE_URI + id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting Conference');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}

angular.module('myApp').factory('ConfService', ['$http', '$q', ($http, $q) => new ConfService($http, $q)]);

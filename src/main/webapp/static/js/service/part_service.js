class PartService {
	
    constructor($http, $q, CONFID) {
    	this.REST_SERVICE_URI = 'http://localhost:8080/participant/';
    	this.REST_SERVICE_URI_CONF = 'http://localhost:8080/conference/';
    	this._$q = $q;
    	this._$http = $http;
    	this._CONFID = CONFID;
    }

    getConf() {
        let deferred = this._$q.defer();
        this._$http.get(this.REST_SERVICE_URI_CONF + this._CONFID)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Conference');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    fetchAllParts() {
        let deferred = this._$q.defer();
        this._$http.get(this.REST_SERVICE_URI + 'byconf/' + this._CONFID)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Participants');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    createPart(part) {
        let deferred = this._$q.defer();
        part.conferenceId = this._CONFID;
        this._$http.post(this.REST_SERVICE_URI, part)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Participant');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }


    updatePart(part, id) {
        let deferred = this._$q.defer();
        this._$http.put(this.REST_SERVICE_URI + id, part)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating Participant');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    deletePart(id) {
        let deferred = this._$q.defer();
        this._$http.delete(this.REST_SERVICE_URI + id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting Participant');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}

angular.module('myApp').factory('PartService', ['$http', '$q', ($http, $q) => new PartService($http, $q, CONFID)]);

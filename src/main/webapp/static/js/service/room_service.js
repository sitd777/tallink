class RoomService {
	
    constructor($http, $q) {
    	this.REST_SERVICE_URI = 'http://localhost:8080/room/';
    	this._$q = $q;
    	this._$http = $http;
    }

    fetchAllRooms() {
        let deferred = this._$q.defer();
        this._$http.get(this.REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Rooms');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    createRoom(room) {
        let deferred = this._$q.defer();
        this._$http.post(this.REST_SERVICE_URI, room)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Room');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }


    updateRoom(room, id) {
        let deferred = this._$q.defer();
        this._$http.put(this.REST_SERVICE_URI + id, room)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating Room');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    deleteRoom(id) {
        let deferred = this._$q.defer();
        this._$http.delete(this.REST_SERVICE_URI + id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting Room');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}

angular.module('myApp').factory('RoomService', ['$http', '$q', ($http, $q) => new RoomService($http, $q)]);

package test.tallink.rest;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import test.tallink.db.Conference;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class ConferenceRestTest {

	@BeforeClass
	public static void setup() {
		String port = System.getProperty("server.port");
		if (port == null) {
			RestAssured.port = Integer.valueOf(8080);
		} else {
			RestAssured.port = Integer.valueOf(port);
		}

		String baseHost = System.getProperty("server.host");
		if (baseHost == null) {
			baseHost = "http://localhost";
		}
		RestAssured.baseURI = baseHost;
		RestAssured.defaultParser = Parser.JSON;
	}

	@Test
	public void testConfsList() {
		given().when().get("/conference").then().statusCode(200);
	}

	@Test
	public void testConfGet() {
		final String confId = "1";

		Conference src = given().contentType("application/json").pathParam("confId", confId).when()
				.get("/conference/{confId}").as(Conference.class);

		assertEquals(src.getId(), confId);
	}

	@Test
	public void testConfCreateDelete() {
		final String roomId = "1";
		final String name = "Conference #1";
		final String date = "12.12.2016";

		Conference conf = new Conference(roomId, name, date, 0, 0);

		Conference res = given().contentType("application/json").body(conf).when().post("/conference")
				.as(Conference.class);

		assertEquals(res.getRoomId(), roomId);
		assertEquals(res.getName(), name);
		assertEquals(res.getDate(), date);

		final String confId = res.getId();

		given().contentType("application/json").pathParam("confId", confId).when().delete("/conference/{confId}").then()
				.statusCode(200);
	}

	@Test
	public void testConfUpdate() {
		final String confId = "1";

		final String name = "Conference #1";
		final String date = "12.12.2016";

		Conference conf = new Conference("0", name, date, 0, 0);

		Conference res = given().contentType("application/json").pathParam("confId", confId).body(conf).when()
				.put("/conference/{confId}").as(Conference.class);

		assertEquals(res.getName(), name);
		assertEquals(res.getDate(), date);
	}

}

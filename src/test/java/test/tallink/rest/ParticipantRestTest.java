package test.tallink.rest;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import test.tallink.db.Participant;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class ParticipantRestTest {

	@BeforeClass
	public static void setup() {
		String port = System.getProperty("server.port");
		if (port == null) {
			RestAssured.port = Integer.valueOf(8080);
		} else {
			RestAssured.port = Integer.valueOf(port);
		}

		String baseHost = System.getProperty("server.host");
		if (baseHost == null) {
			baseHost = "http://localhost";
		}
		RestAssured.baseURI = baseHost;
		RestAssured.defaultParser = Parser.JSON;
	}

	@Test
	public void testPartsList() {
		given().when().get("/participant").then().statusCode(200);
	}

	@Test
	public void testPartGet() {
		final String partId = "1";

		Participant src = given().contentType("application/json").pathParam("partId", partId).when()
				.get("/participant/{partId}").as(Participant.class);

		assertEquals(src.getId(), partId);
	}

	@Test
	public void testPartCreateDelete() {
		final String confId = "1";
		final String fullname = "Participant #1";
		final String birthdate = "28.02.1981";

		Participant part = new Participant(confId, fullname, birthdate);

		Participant res = given().contentType("application/json").body(part).when().post("/participant")
				.as(Participant.class);

		assertEquals(res.getConferenceId(), confId);
		assertEquals(res.getFullname(), fullname);
		assertEquals(res.getBirthdate(), birthdate);

		final String partId = res.getId();

		given().contentType("application/json").pathParam("partId", partId).when().delete("/participant/{partId}")
				.then().statusCode(200);
	}

	@Test
	public void testPartUpdate() {
		final String partId = "1";

		final String confId = "1";
		final String fullname = "Participant #1";
		final String birthdate = "28.02.1981";

		Participant part = new Participant(confId, fullname, birthdate);

		Participant res = given().contentType("application/json").pathParam("partId", partId).body(part).when()
				.put("/participant/{partId}").as(Participant.class);

		assertEquals(res.getFullname(), fullname);
		assertEquals(res.getBirthdate(), birthdate);
	}

}

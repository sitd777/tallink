package test.tallink.rest;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import test.tallink.db.Room;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class RoomRestTest {

	@BeforeClass
	public static void setup() {
		String port = System.getProperty("server.port");
		if (port == null) {
			RestAssured.port = Integer.valueOf(8080);
		} else {
			RestAssured.port = Integer.valueOf(port);
		}

		String baseHost = System.getProperty("server.host");
		if (baseHost == null) {
			baseHost = "http://localhost";
		}
		RestAssured.baseURI = baseHost;
		RestAssured.defaultParser = Parser.JSON;
	}

	@Test
	public void testRoomsList() {
		given().when().get("/room").then().statusCode(200);
	}

	@Test
	public void testRoomGet() {
		final String roomId = "1";

		Room src = given().contentType("application/json").pathParam("roomId", roomId).when().get("/room/{roomId}")
				.as(Room.class);

		assertEquals(src.getId(), roomId);
	}

	@Test
	public void testRoomCreateDelete() {
		final String name = "Test Room #1";
		final String loc = "M/S Baltic Queen";
		final int seats = 12;

		Room room = new Room(name, loc, seats);

		Room res = given().contentType("application/json").body(room).when().post("/room").as(Room.class);

		assertEquals(res.getName(), name);
		assertEquals(res.getLocation(), loc);
		assertEquals(res.getSeats(), seats);

		final String roomId = res.getId();

		given().contentType("application/json").pathParam("roomId", roomId).when().delete("/room/{roomId}").then()
				.statusCode(200);
	}

	@Test
	public void testRoomUpdate() {
		final String roomId = "1";

		final String name = "Conference Room #1.";
		final String loc = "M/S Baltic Queen.";
		final int seats = 80;

		Room room = new Room(name, loc, seats);

		Room res = given().contentType("application/json").pathParam("roomId", roomId).body(room).when()
				.put("/room/{roomId}").as(Room.class);

		assertEquals(res.getName(), name);
		assertEquals(res.getLocation(), loc);
		assertEquals(res.getSeats(), seats);
	}

}

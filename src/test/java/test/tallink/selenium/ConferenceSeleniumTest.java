package test.tallink.selenium;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class ConferenceSeleniumTest {

	private WebDriver driver;

	@Before
	public void setupTest() {
		driver = new FirefoxDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void test() {

		driver.get("http://localhost:8080/conferences/");

		WebElement nameInput = driver.findElement(By.name("name"));
		WebElement dateInput = driver.findElement(By.name("date"));
		WebElement roomIdInput = driver.findElement(By.name("roomId"));
		WebElement submitButton = driver.findElement(By.className("btn-submit"));

		nameInput.sendKeys("Conference #111");
		dateInput.sendKeys("12.12.2016");

		Select select = new Select(roomIdInput);
		select.selectByIndex(1);

		submitButton.click();

		this._wait(2);

		// Check new conference
		WebElement tr = driver
				.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Conference #111"));
		Assert.assertTrue(tr.findElement(By.xpath("td[2]")).getText().equals("12.12.2016"));

		// Modify conference
		WebElement editButton = tr.findElement(By.xpath("td[5]/nobr/button[1]"));
		editButton.click();

		this._wait(2);

		nameInput.sendKeys("...");
		this._wait(2);
		submitButton.click();

		this._wait(2);

		// Check modifications
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Conference #111..."));

		// Delete conference
		WebElement deleteButton = tr.findElement(By.xpath("td[5]/nobr/button[2]"));
		deleteButton.click();

		this._wait(1);

		// Check deletion
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertFalse(tr.findElement(By.xpath("td[1]")).getText().equals("Conference #111..."));
	}

	protected void _wait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (Exception ex) {

		}
	}
}

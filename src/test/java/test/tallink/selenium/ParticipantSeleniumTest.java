package test.tallink.selenium;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class ParticipantSeleniumTest {

	private WebDriver driver;

	@Before
	public void setupTest() {
		driver = new FirefoxDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void test() {

		// Add participant
		driver.get("http://localhost:8080/participants/1");

		WebElement nameInput = driver.findElement(By.name("fullname"));
		WebElement dateInput = driver.findElement(By.name("birthdate"));
		WebElement leftDiv = driver.findElement(By.className("seats-left"));
		WebElement submitButton = driver.findElement(By.className("btn-submit"));

		int left = Integer.parseInt(leftDiv.getText().trim());

		nameInput.sendKeys("Test User");
		dateInput.sendKeys("05.05.1955");

		submitButton.click();

		this._wait(2);

		// Check new participant
		WebElement tr = driver
				.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Test User"));
		Assert.assertTrue(tr.findElement(By.xpath("td[2]")).getText().equals("05.05.1955"));

		// Check left count
		int newleft = Integer.parseInt(leftDiv.getText().trim());
		Assert.assertTrue(newleft == (left - 1));
		this._wait(2);

		// Modify participant
		WebElement editButton = tr.findElement(By.xpath("td[3]/nobr/button[1]"));
		editButton.click();

		this._wait(2);

		nameInput.sendKeys("...");
		submitButton.click();

		this._wait(2);

		// Check modifications
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Test User..."));

		// Delete participant
		WebElement deleteButton = tr.findElement(By.xpath("td[3]/nobr/button[2]"));
		deleteButton.click();

		this._wait(1);

		// Check deletion
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertFalse(tr.findElement(By.xpath("td[1]")).getText().equals("Test User..."));
		int newleft2 = Integer.parseInt(leftDiv.getText().trim());
		Assert.assertTrue(newleft2 == left);
	}

	protected void _wait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (Exception ex) {

		}
	}
}

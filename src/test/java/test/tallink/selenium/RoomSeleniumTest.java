package test.tallink.selenium;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath*:spring/applicationContext.xml" })
public class RoomSeleniumTest {

	private WebDriver driver;

	@Before
	public void setupTest() {
		driver = new FirefoxDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void test() {

		driver.get("http://localhost:8080/rooms/");

		WebElement nameInput = driver.findElement(By.name("name"));
		WebElement locInput = driver.findElement(By.name("location"));
		WebElement seatsInput = driver.findElement(By.name("seats"));
		WebElement submitButton = driver.findElement(By.className("btn-submit"));

		// Create room
		nameInput.sendKeys("Test Room #555");
		locInput.sendKeys("M/S Baltic Queen");
		seatsInput.sendKeys("25"); // Will be 250
		submitButton.click();

		this._wait(2);

		// Check new room
		WebElement tr = driver
				.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Test Room #555"));
		Assert.assertTrue(tr.findElement(By.xpath("td[2]")).getText().equals("M/S Baltic Queen"));
		Assert.assertTrue(tr.findElement(By.xpath("td[3]")).getText().equals("250"));

		// Modify room
		WebElement editButton = tr.findElement(By.xpath("td[4]/button[1]"));
		editButton.click();

		this._wait(2);

		nameInput.sendKeys("...");
		locInput.sendKeys("...");
		submitButton.click();

		this._wait(2);

		// Check modifications
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertTrue(tr.findElement(By.xpath("td[1]")).getText().equals("Test Room #555..."));
		Assert.assertTrue(tr.findElement(By.xpath("td[2]")).getText().equals("M/S Baltic Queen..."));

		// Delete room
		WebElement deleteButton = tr.findElement(By.xpath("td[4]/button[2]"));
		deleteButton.click();

		this._wait(1);

		// Check deletion
		tr = driver.findElement(By.xpath(".//*/div[contains(@class, 'tablecontainer')]/table/tbody/tr[last()]"));

		Assert.assertFalse(tr.findElement(By.xpath("td[1]")).getText().equals("Test Room #555..."));
	}

	protected void _wait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (Exception ex) {

		}
	}
}
